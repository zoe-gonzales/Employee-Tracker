# Employee-Tracker

### This is a straightforward app that intakes employee information and calculates additional info using Moment.js. Each record is saved in Firebase. Styled with Bootstrap and Google Fonts.

### Instructions
* Enter the employee name, job title, start date, and monthly rate. Click submit.
* At this point, the app will calculate how many months the employee has worked and the total billed ($) during that period. All information will be displayed under the _Current Employees_ section.
* Information is retained using Firebase and will not be lost if the page is refreshed, the tab closed, or the browser closed.

Deployed at https://zoe-gonzales.github.io/Employee-Tracker/

![employee tracker](assets/tracker.png)
