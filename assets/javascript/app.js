// Initializing Firebase
var config = {
    apiKey: "AIzaSyBGape3OhO9S48wD0JzW9ZpKLA_IwmZEcg",
    authDomain: "practice-a98c9.firebaseapp.com",
    databaseURL: "https://practice-a98c9.firebaseio.com",
    projectId: "practice-a98c9",
    storageBucket: "practice-a98c9.appspot.com",
    messagingSenderId: "646112298945"
};
firebase.initializeApp(config);
// setting global variables for use later on
var database = firebase.database();
var name = ""; // employee name
var role = ""; // employee title
var startDate = 0; // start date in MM/DD/YYYY format
var monthsWorked = 0; // total months worked to now - calculated later
var monthlyRate = 0; // monthly pay rate
var totalBilled; // total $'s billed to now - calculated later

// FUNCTION DECLARATIONS
// Function gets values from form and saves locally and in Firebase
function addEmployee() {
    // saving input values to variables
    name = $("#employee-name").val().trim();
    role = $("#employee-role").val().trim();
    startDate = $("#start-date").val().trim();
    monthlyRate = $("#monthly-rate").val().trim();
    // saving values in firebase as key-value pairs
    database.ref().push({
        name: name,
        role: role,
        startDate: startDate,
        monthlyRate: monthlyRate,
        dateAdded: firebase.database.ServerValue.TIMESTAMP
    });
}

// Function calculates how many months the employee has worked to now
function calcTimeTilNow(start) {
    var startDateConverted = moment(start, "MM/DD/YYYY");
    monthsWorked = moment().diff(moment(startDateConverted), "months");
}

// Firebase watcher function - runs when data added above
database.ref().on("child_added", function(snapshot){
    // saving snapshot value to variable
    var sv = snapshot.val();
    // update startDate value
    startDate = sv.startDate;
    // calling function to determine the total months worked
    calcTimeTilNow(startDate);
    // calculating total $ billed based on months worked and monthly rate
    totalBilled = monthsWorked * sv.monthlyRate;
    // appending data to the page as additional row in table
    $("#employee-list").append(
    `<tr>  
        <td>${sv.name}</td> 
        <td>${sv.role}</td> 
        <td>${sv.startDate}</td> 
        <td>${monthsWorked}</td>
        <td>${sv.monthlyRate}</td> 
        <td>${totalBilled}</td>
    </tr>
    <hr>`
);
}, function(errorObject) { // These lines run only for errors
  console.log("Errors handled: " + errorObject.code);
});

// CLICK EVENT
// runs addEmployee function when submit button is clicked
$("#add-employee").on("click", addEmployee);